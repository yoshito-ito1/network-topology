from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
import uuid

import network_topology.conf as conf
import network_topology.db.client.file as file_client
import network_topology.db.client.etcd as etcd_client
import network_topology.db.dataclass.utils as utils
import network_topology.exception as exception


CONF = conf.conf()
DATACLASS = CONF['DB']['supported_data_types']
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'


@dataclass
class DataClassBase(ABC):
    uuid: str
    type: str
    created_at: datetime
    updated_at: datetime
    deleted_at: datetime

    @classmethod
    @abstractmethod
    def from_dict(cls, json_dict):
        raise NotImplementedError()

    @abstractmethod
    def to_dict(self):
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def _get_type():
        raise NotImplementedError()

    def create(self):
        db_client = self._get_db_client()
        db_client.create(self)

    def update(self):
        db_client = self._get_db_client()
        data_dict = db_client.read(self._get_type(), self.uuid)
        if self._check_created(data_dict):
            raise exception.NotYetCreatedDataClass(type=self.type,
                                                   uuid=self.uuid)
        if self._check_deleted(data_dict):
            raise exception.AlreadyDeletedDataClass(type=self.type,
                                                    uuid=self.uuid)
        db_client.update(self)

    @classmethod
    def read(cls, uuid):
        db_client = cls._get_db_client()
        data_dict = db_client.read(cls._get_type(), uuid)
        if cls._check_deleted(data_dict):
            return None
        
        parent_uuid = data_dict.get('parent_uuid')
        if parent_uuid:
            return cls.from_dict(data_dict, parent_uuid)
        return cls.from_dict(data_dict)

    def delete(self):
        db_client = self._get_db_client()
        data_dict = db_client.read(self._get_type(), self.uuid)
        if self._check_created(data_dict):
            raise exception.NotYetCreatedDataClass(type=self.type,
                                                   uuid=self.uuid)
        if self._check_deleted(data_dict):
            raise exception.AlreadyDeletedDataClass(type=self.type,
                                                    uuid=self.uuid)
        db_client.delete(self)

    def destroy(self):
        db_client = self._get_db_client()
        db_client.destroy(self)

    @classmethod
    def get_list(cls):
        out = []
        db_client = cls._get_db_client()
        data_dict_list = db_client.get_list(cls._get_type())
        for data_dict in data_dict_list:
            parent_uuid = data_dict.get('parent_uuid')
            if parent_uuid:
                out.append(cls.from_dict(data_dict, parent_uuid))
            else:
                out.append(cls.from_dict(data_dict))
        return out

    def _to_dict(self, result_dict):
        result_dict.update({'uuid': utils.from_str(self.uuid)})
        result_dict.update({'type': utils.from_str(self.type)})
        result_dict.update(
            {'created_at': utils.from_union(
                [utils.from_datetime, utils.from_none], self.created_at)})
        result_dict.update(
            {'updated_at': utils.from_union(
                [utils.from_datetime, utils.from_none], self.updated_at)})
        result_dict.update(
            {'deleted_at': utils.from_union(
                [utils.from_datetime, utils.from_none], self.deleted_at)})
        return result_dict

    @staticmethod
    def _get_uuid(json_dict):
        if json_dict.get('uuid'):
            return utils.from_str(json_dict.get('uuid'))
        else:
            return str(uuid.uuid4())

    @staticmethod
    def _get_timestamp(json_dict):
        created_at = json_dict.get("created_at")
        updated_at = json_dict.get("updated_at")
        deleted_at = json_dict.get("deleted_at")
        out = ()
        for timestamp in [created_at, updated_at, deleted_at]:
            if timestamp:
                timestamp = datetime.strptime(timestamp, DATETIME_FORMAT)
            else:
                pass
            out = out + (timestamp,)
        return out
    
    @staticmethod
    def _check_deleted(json_dict):
        deleted = json_dict.get('deleted_at')
        if deleted:
            return True
        else:
            return False

    @staticmethod
    def _check_created(json_dict):
        created = json_dict.get('created_at')
        if created:
            return False
        else:
            return True

    @staticmethod
    def _get_db_client(db_client_type=None):
        if not db_client_type:
            db_client_type = CONF['DB']['db_client']

        if db_client_type == 'file':
            return file_client.FileClient()
        elif db_client_type == 'etcd':
            return etcd_client.EtcdClient()
        else:
            raise exception.NotSupportedDbClient(db_client_type=db_client_type)
