from dataclasses import dataclass
from typing import Optional, List


from network_topology.db.dataclass.abstract_dataclass import DataClassBase
import network_topology.db.dataclass.utils as utils


@dataclass
class Port(DataClassBase):
    name: str
    interface: str
    parent_uuid: Optional[str]

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        interface = utils.from_str(obj.get("interface"))

        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, interface, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["interface"] = utils.from_str(self.interface)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Port'


@dataclass
class Module(DataClassBase):
    name: str
    ports: Optional[List[Port]]
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        ports = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Port.from_dict, x, uuid),
                utils.from_none],
            obj.get("ports"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, ports, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["ports"] = utils.from_union(
            [lambda x: utils.from_list(lambda x: utils.to_class(Port, x), x),
                utils.from_none],
            self.ports)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Module'


@dataclass
class Slot(DataClassBase):
    name: str
    modules: Optional[List[Module]]
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        modules = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Module.from_dict, x, uuid), utils.from_none],
            obj.get("modules"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, modules, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["modules"] = utils.from_union(
            [lambda x: utils.from_list(lambda x: utils.to_class(Module, x), x),
                utils.from_none],
            self.modules)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Slot'


@dataclass
class Chassis(DataClassBase):
    name: str
    slots: Optional[List[Slot]]
    ports: Optional[List[Port]]
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        slots = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Slot.from_dict, x, uuid), utils.from_none],
            obj.get("slots"))
        ports = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Port.from_dict, x, uuid), utils.from_none],
            obj.get("ports"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, slots, ports, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["slots"] = utils.from_union(
            [lambda x: utils.from_list(lambda x: utils.to_class(Slot, x), x),
                utils.from_none], self.slots)
        result["ports"] = utils.from_union(
            [lambda x: utils.from_list(lambda x: utils.to_class(Port, x), x),
                utils.from_none], self.ports)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Chassis'


@dataclass
class Node(DataClassBase):
    name: str
    chassis: Optional[List[Chassis]]
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        chassis = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Chassis.from_dict, x, uuid), utils.from_none],
            obj.get("chassis"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, chassis, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["chassis"] = utils.from_union(
            [lambda x: utils.from_list(
                lambda x: utils.to_class(Chassis, x), x), utils.from_none],
            self.chassis)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Node'


@dataclass
class Rack(DataClassBase):
    name: str
    total_unit: int
    nodes: Optional[List[Node]]
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        total_unit = utils.from_int(obj.get("total_unit"))
        nodes = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Node.from_dict, x, uuid), utils.from_none],
            obj.get("nodes"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, total_unit, nodes, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["total_unit"] = utils.from_int(self.total_unit)
        result["nodes"] = utils.from_union(
            [lambda x: utils.from_list(lambda x: utils.to_class(Node, x), x),
                utils.from_none], self.nodes)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Rack'


@dataclass
class Socket(DataClassBase):
    number: int
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        number = utils.from_int(obj.get("number"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   number, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["number"] = utils.from_int(self.number)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Socket'


@dataclass
class Outlet(DataClassBase):
    name: str
    sockets: Optional[List[Socket]]
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        assert isinstance(obj, dict)
        name = utils.from_str(obj.get("name"))
        sockets = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Socket.from_dict, x, uuid), utils.from_none],
            obj.get("sockets"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, sockets, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["sockets"] = utils.from_union(
            [lambda x: utils.from_list(lambda x: utils.to_class(Socket, x), x),
                utils.from_none], self.sockets)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Outlet'


@dataclass
class Breaker(DataClassBase):
    name: str
    power_capacity: int
    outlets: Optional[List[Outlet]]
    on: bool
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        power_capacity = utils.from_int(obj.get("power_capacity"))
        outlets = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Outlet.from_dict, x, uuid), utils.from_none],
            obj.get("outlets"))
        on = utils.from_bool(bool(obj.get("on")))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, power_capacity, outlets, on, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["power_capacity"] = utils.from_int(self.power_capacity)
        result["outlets"] = utils.from_union(
            [lambda x: utils.from_list(lambda x: utils.to_class(Outlet, x), x),
                utils.from_none], self.outlets)
        result["on"] = utils.from_bool(self.on)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Breaker'


@dataclass
class SwitchBoard(DataClassBase):
    name: str
    breakers: Optional[List[Breaker]]
    on: bool
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        breakers = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Breaker.from_dict, x, uuid), utils.from_none],
            obj.get("breakers"))
        on = utils.from_bool(bool(obj.get("on")))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, breakers, on, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["breakers"] = utils.from_union(
            [lambda x: utils.from_list(
                lambda x: utils.to_class(Breaker, x), x), utils.from_none],
            self.breakers)
        result["on"] = utils.from_bool(self.on)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'SwitchBoard'


@dataclass
class Room(DataClassBase):
    name: str
    switch_boards: Optional[List[SwitchBoard]]
    racks: Optional[List[Rack]]
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        switch_boards = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                SwitchBoard.from_dict, x, uuid), utils.from_none],
            obj.get("switch_boards"))
        racks = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Rack.from_dict, x, uuid), utils.from_none],
            obj.get("racks"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, switch_boards, racks, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["switch_boards"] = utils.from_union(
            [lambda x: utils.from_list(
                lambda x: utils.to_class(SwitchBoard, x), x), utils.from_none],
            self.switch_boards)
        result["racks"] = utils.from_union(
            [lambda x: utils.from_list(
                lambda x: utils.to_class(Rack, x), x), utils.from_none],
            self.racks)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Room'


@dataclass
class Floor(DataClassBase):
    name: str
    number: int
    rooms: Optional[List[Room]]
    parent_uuid: str

    @classmethod
    def from_dict(cls, obj: dict, parent_uuid: str) -> 'Floor':
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        number = utils.from_int(obj.get("number"))
        rooms = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Room.from_dict, x, uuid), utils.from_none],
            obj.get("rooms"))
        parent_uuid = parent_uuid

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, number, rooms, parent_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["number"] = utils.from_int(self.number)
        result["rooms"] = utils.from_union(
            [lambda x: utils.from_list(
                lambda x: utils.to_class(Room, x), x), utils.from_none],
            self.rooms)
        result["parent_uuid"] = utils.from_str(self.parent_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Floor'


@dataclass
class Building(DataClassBase):
    name: str
    address: str
    company: str
    department: str
    floors: Optional[List[Floor]]

    @classmethod
    def from_dict(cls, obj: dict) -> 'Building':
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        name = utils.from_str(obj.get("name"))
        address = utils.from_str(obj.get("address"))
        company = utils.from_str(obj.get("company"))
        department = utils.from_str(obj.get("department"))
        floors = utils.from_union(
            [lambda x: utils.from_list_with_uuid(
                Floor.from_dict, x, uuid), utils.from_none],
            obj.get("floors"))

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   name, address, company, department, floors)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = utils.from_str(self.name)
        result["address"] = utils.from_str(self.address)
        result["company"] = utils.from_str(self.company)
        result["department"] = utils.from_str(self.department)
        result["floors"] = utils.from_union(
            [lambda x: utils.from_list(
                lambda x: utils.to_class(Floor, x), x), utils.from_none],
            self.floors)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'Building'
