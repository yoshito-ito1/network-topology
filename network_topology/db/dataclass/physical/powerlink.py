from dataclasses import dataclass

from network_topology.db.dataclass.physical.building import Chassis
from network_topology.db.dataclass.physical.building import Socket
from network_topology.db.dataclass.abstract_dataclass import DataClassBase
import network_topology.db.dataclass.utils as utils
from network_topology.exception import NotSupportedOperation
from network_topology.exception import ChassisNotFound, SocketNotFound


@dataclass
class PowerLink(DataClassBase):
    socket_uuid: str
    chassis_uuid: str

    @classmethod
    def from_dict(cls, obj: dict):
        assert isinstance(obj, dict)

        uuid = super()._get_uuid(obj)
        type = cls._get_type()
        created_at, updated_at, deleted_at = super()._get_timestamp(obj)

        socket_uuid = utils.from_str(obj.get("socket_uuid"))
        chassis_uuid = utils.from_str(obj.get("chassis_uuid"))

        cls._check_socket_exists(socket_uuid)
        cls._check_chassis_exists(chassis_uuid)

        return cls(uuid, type, created_at, updated_at, deleted_at,
                   socket_uuid, chassis_uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result['socket_uuid'] = utils.from_str(self.socket_uuid)
        result['chassis_uuid'] = utils.from_str(self.chassis_uuid)
        result = super()._to_dict(result)
        return result

    @staticmethod
    def _get_type():
        return 'PowerLink'

    def update(self):
        raise NotSupportedOperation(data_type=self.type,
                                    method="update()")

    # TODO(yoshito-ito): This method should be more general
    # in DataClassBase like: DataClassBase.check_data_exists()
    @staticmethod
    def _check_chassis_exists(uuid):
        chassis = Chassis.read(uuid)
        if chassis is None:
            raise ChassisNotFound(uuid=uuid)

    # TODO(yoshito-ito): This method should be more general
    # in DataClassBase like: DataClassBase.check_data_exists()
    @staticmethod
    def _check_socket_exists(uuid):
        socket = Socket.read(uuid)
        if socket is None:
            raise SocketNotFound(uuid=uuid)
