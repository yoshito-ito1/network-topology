from network_topology.db.dataclass.physical.building import Building, Floor
from network_topology.db.dataclass.physical.building import Room, Rack, Node
from network_topology.db.dataclass.physical.building import Chassis, Slot
from network_topology.db.dataclass.physical.building import Module, Port
from network_topology.db.dataclass.physical.building import SwitchBoard
from network_topology.db.dataclass.physical.building import Breaker, Outlet
from network_topology.db.dataclass.physical.building import Socket


DATA_CLASS = {
    'Building': Building,
    'Floor': Floor,
    'Room': Room,
    
}


class DbBase:

    def __init__(self):
        pass

    @staticmethod
    def get_building(json_dict) -> Building:
        return Building.from_dict(json_dict)

    @staticmethod
    def read_building(uuid) -> dict:
        return Building.read(uuid)

    @staticmethod
    def get_list_building() -> list[Floor]:
        return Floor.get_list()

    @staticmethod
    def get_floor(json_dict) -> Floor:
        return Floor.from_dict(json_dict)

    @staticmethod
    def read_floor(uuid) -> dict:
        return Floor.read(uuid)

    @staticmethod
    def get_list_floor() -> list[Floor]:
        return Floor.get_list()

    @staticmethod
    def get_room(json_dict) -> Room:
        return Room.from_dict(json_dict)

    @staticmethod
    def read_room(uuid) -> dict:
        return Room.read(uuid)

    @staticmethod
    def get_list_room() -> list[Room]:
        return Room.get_list()

    @staticmethod
    def get_rack(json_dict) -> Rack:
        return Rack.from_dict(json_dict)

    @staticmethod
    def read_rack(uuid) -> dict:
        return Rack.read(uuid)

    @staticmethod
    def get_list_rack() -> list[Rack]:
        return Rack.get_list()

    @staticmethod
    def get_node(json_dict) -> Node:
        return Node.from_dict(json_dict)

    @staticmethod
    def read_node(uuid) -> dict:
        return Node.read(uuid)

    @staticmethod
    def get_list_node() -> list[Node]:
        return Node.get_list()

    @staticmethod
    def get_chassis(json_dict) -> Chassis:
        return Chassis.from_dict(json_dict)

    @staticmethod
    def read_chassis(uuid) -> dict:
        return Chassis.read(uuid)

    @staticmethod
    def get_list_chassis() -> list[Chassis]:
        return Chassis.get_list()


class DbReadOnly(DbBase):

    pass


class DbReadWriteAll(DbBase):

    pass
