import json
import os
import shutil

import network_topology.conf as conf
import network_topology.exception as exception

CONF = conf.conf()
DEFAULT_PATH = CONF['file']['data_path']
DEFAULT_TIMESTAMP = '%Y-%m-%d_%H-%M-%S'


class FileClient:

    def __init__(self, file_path=DEFAULT_PATH):
        self.dir_path = DEFAULT_PATH

    def create(self, dataclass):
        if self._data_already_exist(dataclass):
            raise exception.DuplicateCreateOperation
        else:
            dataclass_str = json.dumps(dataclass.to_dict())
            dir_path = os.path.join(self.dir_path, dataclass.type, dataclass.uuid)
            os.makedirs(dir_path, exist_ok=True)
            created_at = dataclass.created_at.strftime(DEFAULT_TIMESTAMP)
            file_path = os.path.join(dir_path, created_at)
            with open(file_path, 'w') as f:
                f.write(dataclass_str)

    @classmethod
    def read(self, dataclass):
        if dataclass.updated_at:
            timestamp = dataclass.updated_at.strftime(DEFAULT_TIMESTAMP)
        else:
            timestamp = dataclass.created_at.strftime(DEFAULT_TIMESTAMP)
        file_path = os.path.join(self.dir_path, dataclass.type. dataclass.uuid, timestamp)
        with open(file_path, 'r') as f:
            out = f.read()
        return out

    def update(self, dataclass):
        dataclass_str = json.dumps(dataclass.to_dict())
        dir_path = os.path.join(self.dir_path, dataclass.type, dataclass.uuid)
        os.makedirs(dir_path, exist_ok=True)
        update_at = dataclass.update_at.strftime(DEFAULT_TIMESTAMP)
        file_path = os.path.join(dir_path, update_at)
        with open(file_path, 'w') as f:
            f.write(dataclass_str)

    def delete(self, dataclass, force=False):
        dataclass_str = json.dumps(dataclass.to_dict())
        dir_path = os.path.join(self.dir_path, dataclass.type, dataclass.uuid)
        deleted_at = dataclass.deleted_at.strftime(DEFAULT_TIMESTAMP)
        file_path = os.path.join(dir_path, deleted_at)
        if force:
            shutil.rmtree(dir_path)
        else:
            with open(file_path, 'w') as f:
                f.write(dataclass_str)

    @classmethod
    def get_list(self):
        pass

    def _data_already_exist(self, dataclass):
        file_path = os.path.join(self.dir_path, dataclass.type, dataclass.uuid)
        if os.path.exists(file_path):
            return True
        else:
            return False
