from abc import ABC, abstractmethod
import networkx as nx
import plotly.offline as py
import plotly.graph_objs as go


class PlotBase(ABC):

    def __init__(self):
        py.init_notebook_mode(connected=False)
        self.graph = nx.Graph()

    @abstractmethod
    def create(self):
        raise NotImplementedError()

    def _add_edges_from(self, edge_list, weight):
        self.graph.add_edges_from(edge_list, weight=weight)

    def _create_edge_trace(self):
        edge_x = []
        edge_y = []
        for edge in self.graph.edges():
            x = edge[0]
            y = edge[1]
            xposx, xposy = self.pos[x]
            yposx, yposy = self.pos[y]
            edge_x.append(xposx)
            edge_x.append(yposx)
            edge_x.append(None)
            edge_y.append(xposy)
            edge_y.append(yposy)
            edge_y.append(None)

        edge_trace = go.Scatter(
            x=edge_x, y=edge_y,
            line=dict(width=0.5, color='#888'),
            hoverinfo='none',
            mode='lines')

        return edge_trace

    def _create_node_trace(self):
        node_x = []
        node_y = []
        for node in self.graph.nodes():
            x, y = self.pos[node]
            node_x.append(x)
            node_y.append(y)

        node_trace = go.Scatter(
            x=node_x, y=node_y,
            mode='markers',
            hoverinfo='text',
            marker=dict(
                showscale=True,
                # colorscale options
                # 'Greys' | 'YlGnBu' | 'Greens' | 'YlOrRd' | 'Bluered' |
                # 'RdBu' | 'Reds' | 'Blues' | 'Picnic' | 'Rainbow' |
                # 'Portland' | 'Jet' | 'Hot' | 'Blackbody' | 'Earth' |
                # 'Electric' | 'Viridis' |
                colorscale='YlGnBu',
                reversescale=True,
                color=[],
                size=10,
                colorbar=dict(
                    thickness=15,
                    title='Node Connections',
                    xanchor='left',
                    titleside='right'
                ),
                line_width=2))
        return node_trace

    @staticmethod
    def _create_fig(edge_trace, node_trace):
        fig = go.Figure(
            data=[edge_trace, node_trace],
            layout=go.Layout(
                title='<br>Network graph made with Python',
                titlefont_size=16,
                showlegend=False,
                hovermode='closest',
                margin=dict(b=20, l=5, r=5, t=40),
                annotations=[dict(
                    text="Python code: <a href='https://plotly.com/"
                         "ipython-notebooks/network-graphs/'> "
                         "https://plotly.com/ipython-notebooks/network-graphs/"
                         "</a>",
                    showarrow=False,
                    xref="paper", yref="paper",
                    x=0.005, y=-0.002)],
                xaxis=dict(showgrid=False, zeroline=False,
                           showticklabels=False),
                yaxis=dict(showgrid=False, zeroline=False,
                           showticklabels=False),
                height=400,
                width=600)
        )
        return fig
