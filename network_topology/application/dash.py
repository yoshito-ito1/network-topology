import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc

from network_topology.plot.physical_portlink import PlotPhysicalPortLink
from network_topology.plot.physical_powerlink import PlotPhysicalPowerLink


# App Instance
FONTAWESOME = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/'\
              'font-awesome.min.css'
app = dash.Dash(name="Dash name", assets_folder="./application/static",
                external_stylesheets=[dbc.themes.LUX, FONTAWESOME])
app.title = "Test app.title"

# Navbar
navbar = dbc.Nav(className="nav nav-pills", children=[
    # logo/home
    dbc.NavItem(html.Img(src=app.get_asset_url("logo.PNG"), height="40px"))
])

# Input
inputs = dbc.FormGroup([
    html.H4("Select Building"),
    dcc.Dropdown(id="building",
                 options=[{"label": x, "value": x} for x in
                          ["特実棟", "10号館"]],
                 value="特実棟")])

# App Layout
app.layout = dbc.Container(fluid=True, children=[
    # Top
    html.H1("Network Topology View", id="nav-pills"),
    navbar,
    html.Br(), html.Br(), html.Br(),

    # Body
    dbc.Row([
        # input + panel
        dbc.Col(md=3, children=[
            inputs,
            html.Br(), html.Br(), html.Br(),
            html.Div(id="output-panel")
        ]),
        # plots
        dbc.Col(md=9, children=[
            dbc.Col(html.H4("Physical Network Topology"),
                    width={"size": 6, "offset": 3}),
            dbc.Tabs(className="nav nav-pills", children=[
                dbc.Tab(dcc.Graph(id="plot-portlink"), label="PortLink"),
                dbc.Tab(dcc.Graph(id="plot-powerlink"), label="PowerLink")
            ])
        ])
    ])
])


# Python function to plot Portlink view
@app.callback(output=Output("plot-portlink", "figure"),
              inputs=[Input("building", "value")])
def plot_portlink(building):
    plot = PlotPhysicalPortLink()
    return plot.create()


# Python function to plot Powerlink view
@app.callback(output=Output("plot-powerlink", "figure"),
              inputs=[Input("building", "value")])
def plot_powerlink(building):
    plot = PlotPhysicalPowerLink()
    return plot.create()
