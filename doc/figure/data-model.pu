@startuml

title Data model

package NetJson {

    object DeviceConfiguration {
        uuid
        type
        created_at
        updated_at
        deleted_at
        --
        type
        general
        hardware
        operating_system
        radios
        interfaces
        dns_search
        dns_servers
        routes
    }

    object DeviceMonitoring {
        uuid
        type
        created_at
        updated_at
        deleted_at
        --
        type
        general
        resources
        interfaces
    }

    object NetworkCollection {
        uuid
        type
        created_at
        updated_at
        deleted_at
        --
        type      
        collection
    }

    object NetworkGraph {
        uuid
        type
        created_at
        updated_at
        deleted_at
        --
        type
        protocol
        version
        revision
        metric
        router_id
        topology_id
        label
        nodes
        links
    }

    object NetworkRoutes {
        uuid
        type
        created_at
        updated_at
        deleted_at
        --
        type
        protocol
        version
        revision
        metric
        router_id
        routes
    }
}

package "Physical Topology" {

    file building.py {
        object Building {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            address
            company
            department
            floors      
        }

        object Floor {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            number
            rooms
            parent_uuid
        }

        object Room {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            switch_boards
            racks
            parent_uuid
        }

        object Rack {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            total_unit
            nodes
            parent_uuid
        }

        object Node {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            chassis
            parent_uuid
        }

        object Chassis {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            slots
            ports
            parent_uuid
        }

        object Slot {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            modules
            parent_uuid
        }

        object Module {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            ports
            parent_uuid
        }

        object Port {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            interface
            parent_uuid
        }

        object SwitchBoard {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            breakers
            on
            parent_uuid
        }

        object Breaker {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            power_capacity
            outlets
            on
            parent_uuid
        }

        object Outlet {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            name
            sockets
            parent_uuid         
        }

        object Socket {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            number
            parent_uuid        
        }
    }

    file portlink.py {
        object PortLink {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            first_uuid
            second_uuid
        }
    }

    file powerlink.py {
        object PowerLink {
            uuid
            type
            created_at
            updated_at
            deleted_at
            --
            socket_uuid
            chassis_uuid
        }
    }

    Building "1" *-- "N" Floor : floors
    Floor "1" *-- "N" Room: rooms
    Room "1" *-- "N" Rack : racks
    Rack "1" *-- "N" Node : units
    Node "1" *-- "N" Chassis : chassis
    Chassis "1" *-- "N" Slot : slots
    Chassis "1" *-- "1" Port : ports
    Slot "1" *-- "N" Module: modules
    Module "1" *-- "1" Port : port

    Floor "1" *-- "N" SwitchBoard : switchboards
    SwitchBoard "1" *-- "N" Breaker : breaders
    Breaker "1" *-- "N" Outlet : outlets
    Outlet "1" *-- "N" Socket : sockets

    PortLink .> Port : first_uuid\nsecond_uuid
    Socket <. PowerLink : socket_uuid
    Chassis <. PowerLink : chassis_uuid
    '# Node <. Socket : node_uuid
}

Chassis "1" <.. DeviceConfiguration : chassis_uuid
Chassis "1" <.. DeviceMonitoring : chassis_uuid

@enduml